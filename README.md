1.Installation
---
1.1 clone Portal project from bitbucket: git clone https://AlexHolodov@bitbucket.org/AlexHolodov/portal-test.git 

1.2 cd into your project

1.3 Run command:
composer install

1.4 Run command:
cp .env.example .env

1.5 Change the next variables in the .env file:
- APP_URL
- DB_DATABASE=
- DB_USERNAME=
- DB_PASSWORD=
- MAIL_USERNAME=
- MAIL_PASSWORD=

smtp.mailtrap.io is used as smtp server.
Log in on https://mailtrap.io/ and change MAIL_USERNAME and MAIL_PASSWORD in .env to the received ones from mailtrap.

1.6 Generate an app encryption key: php artisan key:generate

1.7 Migrate the database: php artisan migrate

1.8 Test DB dump portal.sql located in the database directory.

2.Description
---
2.1 API endpoints

- api/auth/signup
- api/auth/signup/activate/{token} - process activation by email
- api/auth/login
- api/auth/users/login_link - request login link
- api/auth/check_login_link/{data} - check link and get access to portal home page

>You can import postman collection to check API. The Portal.postman_collection.json file is located in the root directory

2.2 login_link / check_login_link logic

Request login link generates access token, uuid, activation token.
The data is saved to local public storage (storage/app/public) in json format, for example:
```json
{
"email": "dd10@ddd.com",
"accessToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMTMyYjA5NDAyOGYzNzc4YmMzYTY0MTI1MGVjODE3MjM4Yzk5ZmM2YjViN2I3YTJiNmFlNWFmNGM0NzM4Nzg5YjcwNzE2NzUxYTA1NmE5MzkiLCJ
pYXQiOjE1OTQ4MzIxMjgsIm5iZiI6MTU5NDgzMjEyOCwiZXhwIjoxNTk0OTE4NTI1LCJzdWIiOiIiLCJzY29wZXMiOltdfQ.X7DABaPyhFL5H_Lu6ltQG4G2FbJxdGLoRQ2jp2pUPRlv0vlvuzM8yCxyXWc1P0YllxzvrK5jlNPGXOeHbs4HqxoXfo3Ee-iHsSq8Px0v-1YCSdVQPqx
AEWj-v0QItkInQh_pfXdR8rM9gJqO6Lu9jNA5VFnGJ1So_LXdxjeBdxUWOaASW4oC2inosHqUivzZGWFRdTaNAsPZG3KUHHp1GRyqx1OLjEThSFdjSJ3Wp0vICChGdMA3WEIO4n36n9q-hZtU8lYv1ClbPISJuVMmh_hd9vqamWY2Q9Q5W6XjsWIy49cIKdO5xp0vhsiZQN8F52WxTo
8EU7SQEcu4sm4i3E6inHgSidpRyYGSqGR1RA-2MZEvH8HaA9cOjP7RSjdbw4eUm2izcxSNVYHVZvTPTBHUKTCkEXdUDqrzDRq_bc5dEUzDn8JjQQvJEvzLBZejAZx3kjIkoKEiZ3_hwZ4hy-nG9KI-TRv7iJFWjC6ve_Fzw8hS47l-IArp-Pvo5S_KL8D2wV5thUKM_SYrq023L_sQK
XjgmklNE0hxDsU1YQ2DklWiS3FtwTAHhTqWX-sUfoRj-yD3_P4zf-WOQ5meZov0RdY1bI1BDyaNG45lA5Pl2-iMLK3_ciXFSorYmKxHAqg84eQcGEZ_TSggTkbjfuz5stSLNMpjFbi27Genptk",
"uuid":"af491eda-6f90-4562-9cfe-2a1524375f60",
"activation_token":"0bd3ajKL9WRh0M5Nq5vGBgYlNiJxK8sWrXWMBTRAiUvjyswvrX0JRhmTLIWV",
"created_at":"2020-07-15 16:55:28",
}
```

The public disk is intended for files that are going to be publicly accessible. By default, the public disk uses the local driver and stores these files in storage/app/public. To make them accessible from the web, you should create a symbolic link from public/storage to storage/app/public. This convention will keep your publicly accessible files in one directory that can be easily shared across deployments when using zero down-time deployment systems like Envoyer.

To create the symbolic link, you may use the storage:link Artisan command:

php artisan storage:link

Check login link executes the following:

- checks if the file with user info is in storage;
- checks the time passed since the link was sent and if this time is more than maximum allowed it deletes data from storage, otherwise it redirects user to portal page. If the link is valid we can register user in the system.
