<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(
    [
        'prefix' => 'auth',
    ], function () {
    Route::post('login', 'Api\AuthController@login');
    Route::post('/signup', 'Api\AuthController@signup');
    Route::get('signup/activate/{token}', 'Api\AuthController@signupActivate');
    Route::get('check_login_link/{data}', 'Api\AuthController@checkLoginLink');
    Route::group(
        [
            'middleware' => 'auth:api',
        ], function () {
        Route::get('users', 'Api\UserController@index');
        Route::get('users/login_link', 'Api\UserController@requestLoginLink');
    }
    );
}
);

