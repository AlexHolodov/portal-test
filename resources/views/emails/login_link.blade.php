<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Confirm login link</title>
</head>
<body>
<h1>Login link confirmation</h1>
<p>Please confirm your login link: <a href="{{$url}}">Confirm</a></p>
<p>Thank you for using our application!</p>
</body>
</html>
