<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PortalController extends Controller
{
    public function homePage($email)
    {
        return view('home_page', [$email]);
    }
}
