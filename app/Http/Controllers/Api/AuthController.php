<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Notifications\SignupActivate;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class AuthController extends Controller
{
    public function signup(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:55',
            'email' => 'email|required|unique:users',
            'password' => 'required|confirmed',
        ]);

        $validatedData['password'] = bcrypt($request->password);
        $validatedData['activation_token'] = str_random(60);
        $user = User::create($validatedData);

        $accessToken = $user->createToken('authToken')->accessToken;
        $user->notify(new SignupActivate($user));

        return response([ 'user' => $user, 'access_token' => $accessToken]);
    }

    public function login(Request $request)
    {
        $loginData = $request->validate([
            'email' => 'email|required',
            'password' => 'required',

        ]);
        $loginData['active'] = 1;
        $loginData['deleted_at'] = null;
        if (!auth()->attempt($loginData)) {
            return response(['message' => 'Invalid Credentials'], 401);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;
        $uuid = Str::uuid()->toString();
        return response(['user' => auth()->user(), 'access_token' => $accessToken]);

    }

    public function signupActivate($token)
    {
        $user = User::where('activation_token', $token)->first();
        if (!$user) {
            return response()->json([
                'message' => 'This activation token is invalid.'
            ], 404);
        }
        $user->active = true;
        $user->activation_token = '';
        $user->save();
        return response()->json([
            'user' => $user,
            'message' => 'User activated.'
        ]);
        return $user;
    }

    public function checkLoginLink($data)
    {
        $requestData = explode('---',$data);
        $uuid = $requestData[0];
        $token = $requestData[1];

        $user = User::where('activation_token', $token)->first(); // check user exists if registration is required
        if (!$user) {
            if (Storage::disk('public')->has($uuid . '.json')) {
                $userData = json_decode(Storage::disk('public')->get($uuid . '.json'));

                // check user data
                if ($uuid == $userData->uuid && $token == $userData->activation_token) {
                    // check if link is expired
                    $date = Carbon::parse($userData->created_at)->addMinutes(config('auth.email_verification_time_limit'));

                    $minutes = $date->diffInMinutes(now());

                    if ($minutes > config('auth.email_verification_time_limit')) {
                        Storage::disk('public')->delete($uuid . '.json');
                        return response([
                            'message' => 'This link has expired, please request a new one.'
                        ], 404);
                    }
                    // Access portal home page if email verified
                    return view('home_page', [ 'email' => $userData->email]);
                } else {
                    return response([
                        'message' => 'This access token is invalid.'
                    ], 404);
                }

            } else { // login user
                return response()->json([
                    'message' => 'Forbidden.'
                ], 404);
            }
        }


    }

    public function index() {

    }
}
