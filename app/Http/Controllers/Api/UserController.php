<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\SendLoginLink;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Response;

class UserController extends Controller
{
    public function index(){
        if (Auth::guard('api')->check()) {
            $users = User::all();
            return response(['users' => $users]);
        }
        return response('Unauthenticated user');
    }

    public function requestLoginLink(Request $request) {
        $content =  json_decode($request->getContent());
        if (!$content) {
            return response(['error' => 'Email address is required.']);
        }
        $email = $content->email;
        $uuid = (string) Str::uuid();
        $user = new User();
        $access_token = $user->createToken('authToken')->accessToken;
        $activation_token = str_random(60);
        $storeData = [
            'email' => $email,
            'accessToken' => $access_token,
            'uuid' => $uuid,
            'activation_token' => $activation_token,
            'created_at' => date('Y-m-d H:i:s')
        ];
        Storage::disk('public')->put($uuid . '.json', json_encode($storeData));

        $url = url('/api/auth/check_login_link/'. $uuid . '---' . $activation_token);
        Mail::to($storeData['email'])->send(new SendLoginLink($url));

        return response(['link_data' => $storeData]);

    }
}
