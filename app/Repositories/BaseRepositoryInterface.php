<?php

namespace App\Repositories;

/**
 * Interface Repository
 * @package Api\Repositories
 */
interface BaseRepositoryInterface
{
    /**
     * @param  int $id
     * @return $model
     */
    public function find($id);

    /**
     * Return a collection of all elements of the resource
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all();

    /**
     * Create a resource
     * @param  $data
     * @return $model
     */
    public function create($data);

    /**
     * Update a resource
     * @param  $model
     * @param  array $data
     * @return $model
     */
    public function update($model, $data);

    /**
     * Destroy a resource
     * @param  $model
     * @return bool
     */
    public function destroy($model);

    /**
     * Return a collection of elements who's ids match
     * @param  array $ids
     * @param  string $column
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findByIds(array $ids, $column = 'id');

    /**
     * Delete all table entries for this Repositories' Entity
     * @return bool
     */
    public function deleteAll();


}
