<?php
namespace App\Repositories\Eloquent;

use Api\Repositories\Eloquent\EloquentBaseRepository;
use App\Models\User;
use Illuminate\Support\Collection;
use App\Repositories\UserRepositoryInterface;
use Api\Repositories\BaseRepositoryInterface;

class EloquentUserRepository extends EloquentBaseRepository implements UserRepositoryInterface
{
    /**
     * Returns all the users
     * @return object
     */
    public function all() {
        return $this->model->all();
    }

    /**
     * Create a user
     * @param  array $data
     * @return mixed
     */
    public function create(array $data) {

    }

    /**
     * Find a user by its ID
     * @param $id
     * @return mixed
     */
    public function find($id) {

    }

    /**
     * Update a user
     * @param $user
     * @param $data
     * @return mixed
     */
    public function update($user, $data) {

    }

    /**
     * Deletes a user
     * @param $id
     * @return mixed
     */
    public function delete($id) {

    }
}
