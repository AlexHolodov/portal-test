<?php

namespace App\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Api\Repositories\BaseRepository;

/**
 * Class EloquentCoreRepository
 *
 * @package Modules\Core\Repositories\Eloquent
 */
abstract class EloquentBaseRepository implements BaseRepositoryInterface
{
    /**
     * @var \Illuminate\Database\Eloquent\Model An instance of the Eloquent Model
     */
    protected $model;

    /**
     * @param Model $model
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * @inheritdoc
     */
    public function find($id)
    {
        if (method_exists($this->model, 'translations')) {
            return $this->model->with('translations')->find($id);
        }

        return $this->model->find($id);
    }

    public function all()
    {
        $query = $this->model->query();

        if(array_key_exists('created_at', $this->model->attributesToArray())) {
            $query = $query->orderBy('created_at', 'DESC');
        }

        return $query->get();
    }

    public function create($data)
    {
        return $this->model->create($data);
    }

    public function update($model, $data)
    {
        $model->update($data);

        return $model;
    }

    /**
     * @inheritdoc
     */
    public function destroy($model)
    {
        return $model->delete();
    }


    public function findByIds(array $ids, $column = 'id', $orderBy = null, $sortOrder = 'asc')
    {
        $query = $this->model->query();

        if (null !== $orderBy) {
            $query->orderBy($orderBy, $sortOrder);
        }

        return $query->whereIn($column, $ids)->get();
    }

    /**
     * @inheritdoc
     */
    public function deleteAll()
    {
        return $this->model->query()->delete();
    }

}
