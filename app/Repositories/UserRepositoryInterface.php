<?php

namespace App\Repositories;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Interface UserRepository
 * @package Modules\User\Repositories
 */
interface UserRepositoryInterface
{
    /**
     * Returns all the users
     * @return object
     */
    public function all();

    /**
     * Create a user
     * @param  array $data
     * @return mixed
     */
    public function create(array $data);

    /**
     * Find a user by its ID
     * @param $id
     * @return mixed
     */
    public function find($id);

    /**
     * Update a user
     * @param $user
     * @param $data
     * @return mixed
     */
    public function update($user, $data);

    /**
     * Deletes a user
     * @param $id
     * @return mixed
     */
    public function delete($id);

}
