-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: localhost    Database: portal
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2016_06_01_000001_create_oauth_auth_codes_table',1),(3,'2016_06_01_000002_create_oauth_access_tokens_table',1),(4,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),(5,'2016_06_01_000004_create_oauth_clients_table',1),(6,'2016_06_01_000005_create_oauth_personal_access_clients_table',1),(7,'2019_08_19_000000_create_failed_jobs_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('02276e87585484c662d0e7a076c937ff273d37808604771b18795b7e5868e1b8090cb5e7356779f0',NULL,1,'authToken','[]',0,'2020-07-15 06:21:24','2020-07-15 06:21:24','2020-07-16 09:21:19'),('040fa4e5627d247b7f4ec0d8f62bd50f8834c29bcc7abb20dcee36d092fc2ba75c1952e60f245506',NULL,1,'authToken','[]',0,'2020-07-15 11:17:06','2020-07-15 11:17:06','2020-07-16 14:17:02'),('094e27771f91d66e7057d94f26c0f6a82f523257ff265746175fbbf01163e32ab2130f989659aa66',NULL,1,'authToken','[]',0,'2020-07-15 06:46:04','2020-07-15 06:46:04','2020-07-16 09:45:59'),('0ef0f2c552858875093675c5854217105a4467b872145c27e967839f4f943706cf87594bebdaceaa',NULL,1,'authToken','[]',0,'2020-07-15 07:49:29','2020-07-15 07:49:29','2020-07-16 10:49:26'),('132b094028f3778bc3a641250ec817238c99fc6b5b7b7a2b6ae5af4c4738789b70716751a056a939',NULL,1,'authToken','[]',0,'2020-07-15 13:55:28','2020-07-15 13:55:28','2020-07-16 16:55:25'),('15a7ce47b5fec6b55b418998a00c6cdf065aba93f2c4ad626e3ae20667b32c8a6f2dd674be67dfa1',11,1,'authToken','[]',0,'2020-07-15 13:56:14','2020-07-15 13:56:14','2020-07-16 16:56:03'),('176adc7ae836d3264c617f0a967e1d3c4f6369024e6e90ab9b359af46558801cf76f40929a7f6d89',5,1,'authToken','[]',0,'2020-07-15 13:13:21','2020-07-15 13:13:21','2020-07-16 16:13:16'),('19ef32f126d168a94eb9ae826c934b512817a9b784914117b62f20e0c28e5f9406cfb7943cea75f6',NULL,1,'authToken','[]',0,'2020-07-15 13:49:01','2020-07-15 13:49:01','2020-07-16 16:48:58'),('28a454ab0d8800eae56512a7affbf733f41b4eacd5729dfbac86f9357e329b05251ea70f126ae6a4',3,1,'authToken','[]',0,'2020-07-15 03:25:44','2020-07-15 03:25:44','2020-07-16 06:25:44'),('32609c0e97c0da6443df3c56466936ed745944b0b2709217c80a6fb4b5dee19673ba94cd558460e5',NULL,1,'authToken','[]',0,'2020-07-15 09:03:41','2020-07-15 09:03:41','2020-07-16 12:03:37'),('369bcdfbe0ff3ac46633b078bbf01ffb8a31b2b1e11f524a175d8f4c87aa1cbb1e62819cc5e47764',NULL,1,'authToken','[]',0,'2020-07-15 06:39:36','2020-07-15 06:39:36','2020-07-16 09:39:30'),('4539236111af4b69af1e5d43e8a1bd4ab934b32e281254e3fd37b689e0217b47937d9e1173d62d90',NULL,1,'authToken','[]',0,'2020-07-15 09:39:32','2020-07-15 09:39:32','2020-07-16 12:39:28'),('59ce7a422ceb52ca7285aea53649e50ae180300abd643677d1f40d6ef3f8521474d06369232e6f3a',NULL,1,'authToken','[]',0,'2020-07-15 05:56:20','2020-07-15 05:56:20','2020-07-16 08:56:16'),('643ba90c685c2a7ea76b50b91f1507e94fa7ea3f9b27416c93e11f353b73a834b6385ce2f232b7d7',NULL,1,'authToken','[]',0,'2020-07-15 09:28:53','2020-07-15 09:28:53','2020-07-16 12:28:42'),('785d975488821a0ac7134c988d4765636485722a7a3e9fdbf144628efb7c79c00670c455b476cb18',10,1,'authToken','[]',0,'2020-07-15 13:47:38','2020-07-15 13:47:38','2020-07-16 16:47:34'),('7fd491c28aef7c460952565ad132cdcea8ca77ae217857608ffefe129d45afcee73355b786d1a7cc',3,1,'authToken','[]',0,'2020-07-15 13:56:43','2020-07-15 13:56:43','2020-07-16 16:56:42'),('8171217927fbed2af85e9991faab63dabc3025a2bda7178ffa4a6c90b48c2ee924271c3310cb236d',NULL,1,'authToken','[]',0,'2020-07-15 07:21:03','2020-07-15 07:21:03','2020-07-16 10:20:51'),('89b15cf784676671241c294c17e59fe183f9aabedfa198cf3980430c6a9fe50e64f081dff45a7858',6,1,'authToken','[]',0,'2020-07-15 13:36:51','2020-07-15 13:36:51','2020-07-16 16:36:39'),('910f24595bd80902123f99052032706a9d033d7b04e92af2e40ffbb0372aca63c3ad4104e6c67023',NULL,1,'authToken','[]',0,'2020-07-15 07:20:14','2020-07-15 07:20:14','2020-07-16 10:20:08'),('92bad6e2229d09993e4c6f69eeb9985048a11b3dfa547e95cfcfe4dcbc379189eb4987e1476316c6',NULL,1,'authToken','[]',0,'2020-07-15 07:28:14','2020-07-15 07:28:14','2020-07-16 10:28:10'),('9997e0c0621609e8971120305bd14a8c682c662d318d2f7d822e418d99357a5f6784038cdd72b412',NULL,1,'authToken','[]',0,'2020-07-15 09:29:46','2020-07-15 09:29:46','2020-07-16 12:29:42'),('bce8372c3398bbd653cc1f0164e6db0aaa9d7176aafab5515f440ca96e820e1b180c50f19aaf6968',NULL,1,'authToken','[]',0,'2020-07-15 06:16:03','2020-07-15 06:16:03','2020-07-16 09:15:55'),('c13cc4e4f9d2775dda1fa1795557e7055a8cfa5746f6254654d8fdadd51b1c7d4637b326106a119b',NULL,1,'authToken','[]',0,'2020-07-15 07:31:58','2020-07-15 07:31:58','2020-07-16 10:31:46'),('c46bc7618154064f456c7c1fd471c3dfca15c6020ba8e3b5fd8cc0f1dd07832774bbed3ef609bb6a',3,1,'authToken','[]',0,'2020-07-15 03:00:50','2020-07-15 03:00:50','2020-07-16 06:00:46'),('ca16898824ff5993ea299d4d3d786cf328550f1694b988616e1eddd4b2f823f9b66af2a654fa2d0c',NULL,1,'authToken','[]',0,'2020-07-15 09:11:45','2020-07-15 09:11:45','2020-07-16 12:11:42'),('cb67e97c8606a38edcb105ae9e3d2bba2ee4d79ac147880fcee826da0062ee91b432f2e2181a4c3c',8,1,'authToken','[]',0,'2020-07-15 13:40:25','2020-07-15 13:40:25','2020-07-16 16:40:18'),('d5ce21033f5fd64eeb839aefdc4d3f3add7655407028c9adf4b99a22b92eb76927b9a4b7c2a60312',NULL,1,'authToken','[]',0,'2020-07-15 06:46:55','2020-07-15 06:46:55','2020-07-16 09:46:51'),('d69896c2ee205bc188c2cef95b5f68a9af97fb263cca9b33abd2b00050c0723390976457bd768bad',9,1,'authToken','[]',0,'2020-07-15 13:42:53','2020-07-15 13:42:53','2020-07-16 16:42:47'),('d6b37bc16874510153c11d8a587479cc35e7b35462830a6949391c84950f9a4d466c0a99fdc92d10',7,1,'authToken','[]',0,'2020-07-15 13:38:59','2020-07-15 13:38:59','2020-07-16 16:38:54'),('d7510e4f812c50b6264dcfccc7df56075f5b9702f7deeddb9fedbbbaa7bd95807ed114bbaf7b1d5c',NULL,1,'authToken','[]',0,'2020-07-15 07:26:46','2020-07-15 07:26:46','2020-07-16 10:26:39'),('db0e8c2e8b282b1ebc77b8455d0b5d87871bac22263645709dc90f2956711d241f81d870d323eb13',NULL,1,'authToken','[]',0,'2020-07-15 06:16:55','2020-07-15 06:16:55','2020-07-16 09:16:51'),('db504420d0ea094a766400f316618ea362c5b9f5c3f529f33d400973619d68b3839731f55ccdcd46',4,1,'authToken','[]',0,'2020-07-15 09:23:47','2020-07-15 09:23:47','2020-07-16 12:23:35'),('e7fe874cd3a25fae747828007bb1f8d217a1e30f342bf174f26fe712c74a0a9d016fe3da6768541a',NULL,1,'authToken','[]',0,'2020-07-15 06:14:59','2020-07-15 06:14:59','2020-07-16 09:14:52'),('ea92ccc16441d5975ed88db6f8523e8c392209ab8a35ebac5aeb89c7cd6e197ee3470ccd97af76f5',NULL,1,'authToken','[]',0,'2020-07-15 11:46:21','2020-07-15 11:46:21','2020-07-16 14:46:17'),('f94484ee02571701e406051e5b2c8fc6fb2147ac811822bd7af26ae1a73e49530587ce252f8599fe',NULL,1,'authToken','[]',0,'2020-07-15 07:25:27','2020-07-15 07:25:27','2020-07-16 10:25:21'),('fbb1f10f08dd534dc27a370eea8370f54cd0956d62e84abf570b3fd7093d3fefde8a53ca90cc75d9',NULL,1,'authToken','[]',0,'2020-07-15 09:12:17','2020-07-15 09:12:17','2020-07-16 12:12:08'),('fd7c01d135f2e742792ad5df1dfa2d4fbf1dbd3e598f9fea2b7bf0ccac5634a3482a257f7d22b5d1',NULL,1,'authToken','[]',0,'2020-07-15 08:51:21','2020-07-15 08:51:21','2020-07-16 11:51:13');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES (1,NULL,'Laravel Personal Access Client','URE1MOSHS5u8FLNWIRfOjAq0NPFTMgZz8A6DSuf7',NULL,'http://localhost',1,0,0,'2020-07-15 02:59:22','2020-07-15 02:59:22'),(2,NULL,'Laravel Password Grant Client','sfvupO6Mokqu80KQGV6ew3PsLfJPAphPH7qABbm5','users','http://localhost',0,1,0,'2020-07-15 02:59:22','2020-07-15 02:59:22');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` VALUES (1,1,'2020-07-15 02:59:22','2020-07-15 02:59:22');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `activation_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'John  Doe','jd1@dddcom',NULL,'$2y$10$lpzt2LQsU0GNZ8li4jEPfOK8tXePxekWgSB.3zG51H.A2Wqrl8hAm',0,'GjEgaCSpOCxFlS5bk3YQYebzokhcV0v81rBLQx3AwRvbNJ2ItNyh34TbJMLE',NULL,NULL,'2020-07-15 02:54:48','2020-07-15 02:54:48'),(2,'John  Doe','jd2@dddcom',NULL,'$2y$10$3UaknTkLipYhauveVEavSeozEvkiiXvT2Y9Ic8uDLJkt.OL1tkHpK',0,NULL,NULL,NULL,'2020-07-15 02:57:21','2020-07-15 02:57:21'),(3,'John  Doe','jd3@dddcom',NULL,'$2y$10$UZcnOiU9F1U9AfQYjziJl.jeRc0I1RzY1xHhmjY0byB.7Bt/Y46z2',1,'',NULL,NULL,'2020-07-15 03:00:50','2020-07-15 03:06:26'),(4,'John  Gray','jd33@dddcom',NULL,'$2y$10$tCtpHfAT4k7JgbhiweMt1edRuchw4TTkNgjpccmpnutcn982457eG',0,'YDZtrgR801H4B60NpRfp2qqX6iRX3jQNQCiWvzRHYO7xGqaoFLPxXGGpCwDX',NULL,NULL,'2020-07-15 09:23:46','2020-07-15 09:23:46'),(5,'John  Gray','jd333@dddcom',NULL,'$2y$10$p.1AzQM3hw.h4AAvClyiEO/z4d4L7jyBgckgnaVwTYYRCTJ/jBO9G',0,'5IXbbjjjSuegPx3osvpO0FwGcePIbydDlX1WY7SFLiB7nJje1sYbkVP1494P',NULL,NULL,'2020-07-15 13:13:20','2020-07-15 13:13:20'),(6,'John  Gray','jd334@dddcom',NULL,'$2y$10$BgCjYO78ZDz.XkP4y1byn.Tg74T9i49xxbYBs/bDbmf6.NB8B.Sg2',0,'Avn6QSB8CqUiNzD3jZiWr85krFcFHsCvB4GNmaLD0WzexLQsnUMh8eCnpqMJ',NULL,NULL,'2020-07-15 13:36:50','2020-07-15 13:36:50'),(7,'John  Gray','jd335@dddcom',NULL,'$2y$10$35O1wUeaeeynYLD0hw2vO.HEHTFofuW2xzTTHEaOllo1ajviCUSia',0,'yCm0InawlZT7PrgYnceRaZwJ8lcGKiXsLia3NM4WaGqBMChBBiwa4vfFVHCG',NULL,NULL,'2020-07-15 13:38:59','2020-07-15 13:38:59'),(8,'John  Gray','jd337@dddcom',NULL,'$2y$10$NLzxQnUngWNzDHGFHifvjegb.fgrXYO6ZfwYhJCZmIZplpRZzS4cO',0,'cfDx8zqmJya9Y0K4l2yt57C55hLZR7lpHvqIEM1AgNN6JHchlrfjKtqIR4pP',NULL,NULL,'2020-07-15 13:40:25','2020-07-15 13:40:25'),(9,'John  Gray','jd339@dddcom',NULL,'$2y$10$ODNin790s3G9W3HorcBwSevq3V0jymmKRohNDxxqy9byLnBu5k22a',0,'NeuRcqGE3w38YULOIn2VmoyNcmxuCSYXaIGRmGZkpwIamJ2WINhABr0GrY9F',NULL,NULL,'2020-07-15 13:42:53','2020-07-15 13:42:53'),(10,'John  Gray','jd319@dddcom',NULL,'$2y$10$vTItSeimeRFnWnrgYYZnCuybij8NYqpROCUkzJvakVMg0ZlDNqwRa',0,'7zIzie5wIcHZM0q0bqux3G1YHOb5vCKSF1p3yqYoJbZyLryq6iexGcskvBkj',NULL,NULL,'2020-07-15 13:47:38','2020-07-15 13:47:38'),(11,'John  Gray','jd320@dddcom',NULL,'$2y$10$6JzQxW9QAw9WdUSW.tjGGeUS2PJ5cmkTqETUGNnFgclwQX9gHcKUa',0,'y7k8EBHv4K1qWOxjuNd4TcQX0Ehae0rb7uLb88kjyXKHJHi1ogGKP1KMkfXe',NULL,NULL,'2020-07-15 13:56:14','2020-07-15 13:56:14');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-16 10:55:37
